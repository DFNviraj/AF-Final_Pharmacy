'use strict';

import React, {Component} from 'react';
import axios from 'axios';
var url = require('../../Statics.Common');

export default class UpdateDrugController extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            users: []
        }
            this.getAllUsers();
        
    }

   onSubmit(event){
        event.preventDefault();
        this.props.updateDrug(
            this.drugid.value,
            this.drugname.value,
            this.drugqty.value,
            this.remark.value,
            this.drugtype.value,
            this.drugcreatedate.value,
            this.drugdosage.value,
            this.drugcreatedby.value,
            this.drugexpiration.value,
            this.drugprice.value,
            
        );
        this.drugid.value='';
        this.drugname.value='';
        this.drugqty.value='';
        this.remark.value='';
        this.drugtype.value='';
        this.drugcreatedate.value='';
        this.drugdosage.value='';
        this.drugcreatedby.value='';
        this.drugexpiration.value='';
        this.drugprice.value='';
        alert('Drug Updated Successfully');
        
   }

    getAllUsers() {
        axios.get(url.node + `drug`).then(res => {
            this.setState({
                users: res.data.data || res.data
            });
        })
    }

    render() {
        return <div>
           <br/><br/><div class="ui grid" align="left">
            <div class="ui blue icon message">
  <i class="wheelchair icon"></i>
  <div class="content">
    <div class="header">
    Update Existing Drugs
    </div>
    <p>Good health is not something we can buy...</p>
  </div>
</div>

            <form class="ui  form">
            {/* <div class="inline fields"> */}
            <table class="ui  large teal celled table">

             <tr><div class="inline field"><td><label>Drug ID</label>
             <input placeholder="DrugID" ref={drugid=>this.drugid=drugid}/></td></div>
             </tr>
            
            
             <tr>
            <div class="inline field"><td><label>Drug Name</label>
            <input  placeholder="Drug Name" ref={drugname=>this.drugname=drugname}/></td></div> 
            </tr>

            <tr>
            <div class="inline field"><td><label>Quantity</label>
            <input type="number" placeholder="Quantity" ref={drugqty=>this.drugqty=drugqty}/></td></div>    
            </tr>         

             <tr>
            <div class="inline field"><td><label>Remark</label>
            <input  placeholder="Remark" ref={remark=>this.remark=remark}/></td></div>    
            </tr>     

              <tr>     
            <div class="inline field"><td><label>Drug Type</label>
            <input  placeholder="Drug Type" ref={drugtype=>this.drugtype=drugtype}/></td></div>  
            </tr>       
            <tr>     
            <div class="inline field"><td><label>Created Date</label>
            <input placeholder="Created Date" ref={drugcreatedate=>this.drugcreatedate=drugcreatedate}/></td></div>              
            </tr>

               <tr>   
            <div class="inline field"><td><label>Drug Dosage</label>
            <input type="number"  placeholder="Drug Dosage" ref={drugdosage=>this.drugdosage=drugdosage}/></td></div>
            </tr>
            
            <tr>
            <div class="inline field"><td><label>Drug Created</label>
            <input  placeholder="Drug Created" ref={drugcreatedby=>this.drugcreatedby=drugcreatedby}/></td></div>
            </tr>
                
            <tr><div class="inline field"><td><label>Drug Expiration</label>
            <input  placeholder="Drug Expiration" ref={drugexpiration=>this.drugexpiration=drugexpiration}/></td></div>
            </tr>

            <tr><div class="inline field"><td><label>Drug Price</label>
            <input  placeholder="Drug Price" ref={drugprice=>this.drugprice=drugprice}/></td></div>
            </tr>

                  <tr>
            <div class="inline field"><td><button  align="center" class="ui primary button" onClick={this.onSubmit}>Update Drugs</button></td></div>  </tr>
            {/* </div> */}</table>
            </form>


            
            </div>
            {/* </div>      */}
                       
            </div>
        
    }
}