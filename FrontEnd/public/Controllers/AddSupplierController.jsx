'use strict';

import React, {Component} from 'react';
import axios from 'axios';
var url = require('../../Statics.Common');

export default class AddSupplierController extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      users: []
    }
    this.getAllUsers();

  }

  onSubmit(event){
    event.preventDefault();
    this.props.addSupplier(


      this.supplierid.value,
      this.suppliername.value,
      this.suppliercompany.value,

      this.supplieremail.value,

      this.suppliercontactno.value,
      this.supplieraddress.value

    );
    this.supplierid.value='';
    this.suppliername.value='';
    this.suppliercompany.value='';

    this.supplieremail.value='';

    this.suppliercontactno.value='';
    this.supplieraddress.value='';
    alert('Supplier Added Successfully');



  }

  getAllUsers() {
    axios.get(url.node + `supplier`).then(res => {
      this.setState({
        users: res.data.data || res.data
      });
    })
  }

  render() {
    return <div>
      <br/><br/><div class="ui grid" align="left">
      <div class="ui blue icon message">
  <i class="user icon"></i>
  <div class="content">
    <div class="header">
    View Details of the Suppliers
    </div>
    <p>To enjoy the glow of good health, you must exercise...

</p>
  </div>
</div>


      <form class="ui  form">
        {/* <div class="inline fields"> */}

        <div class="inline field"><label>Supplier ID</label><input placeholder="Supplier ID" ref={supplierid=>this.supplierid=supplierid}/></div>
        <div class="inline field"><label>Supplier Name</label><input  placeholder="Supplier Name" ref={suppliername=>this.suppliername=suppliername}/></div>
        <div class="inline field"><label>Supplier Company</label><input  placeholder="Supplier Company" ref={suppliercompany=>this.suppliercompany=suppliercompany}/></div>

        <div class="inline field"><label>Supplier Email</label><input  placeholder="Supplier Email" ref={supplieremail=>this.supplieremail=supplieremail}/></div>

        <div class="inline field"><label>Supplier contact no</label><input  placeholder="Supplier contact no" ref={suppliercontactno=>this.suppliercontactno=suppliercontactno}/></div>
        <div class="inline field"><label>Supplier Address</label><input  placeholder="Supplier Address" ref={supplieraddress=>this.supplieraddress=supplieraddress}/></div>



        <div class=" field"><button  class="ui primary button" onClick={this.onSubmit}>Add Supplier</button>  <button class="ui secondary button">Reset</button></div>


        {/* </div> */}
      </form>
    </div>



<br/><br/><br/><br/><br/>

    </div>

  }
}
