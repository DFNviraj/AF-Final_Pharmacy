'use strict';

import React, {Component} from 'react';
import axios from 'axios';
var url = require('../../Statics.Common');

export default class AddRequestController extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            users: []
        }
            this.getAllUsers();
        
    }

   onSubmit(event){
        event.preventDefault();
        this.props.addRequest(
            this.drugid.value,
            this.drugName.value,
            this.requestedQty.value,
            this.department.value,
            this.status.value,
            this.category.value

            
        );
        this.drugid.value='';
        this.drugName.value='';
        this.requestedQty.value='';
        this.department.value='';
        this.status.value='';
        this.category.value='';
        alert('Request Added Successfully');
        
   }

    getAllUsers() {
        axios.get(url.node + `drug`).then(res => {
            this.setState({
                users: res.data.data || res.data
            });
        })
    }

    render() {
        return <div>
            <br/><br/><div class="ui grid" align="left">
            <div class="ui blue icon message">
  <i class="calendar check outline icon"></i>
  <div class="content">
    <div class="header">
    Add New Requests
    </div>
    <p>Early to bed and early to rise makes a man healthy, wealthy and wise...</p>
  </div>
</div>



            <form class="ui  form">
            {/* <div class="inline fields"> */}
            
            <div class="inline field"><label>Drug ID</label><input placeholder="Drug ID" ref={drugid=>this.drugid=drugid}/></div>
            <div class="inline field"><label>Drug Name</label><input  placeholder="Drug Name" ref={drugName=>this.drugName=drugName}/></div>              
            <div class="inline field"><label>Requested Quantity</label><input type="number" placeholder="Requested Quantity" ref={requestedQty=>this.requestedQty=requestedQty}/></div>              
            <div class="inline field"><label>Department</label><input  placeholder="Department" ref={department=>this.department=department}/></div>             
            <div class="inline field"><label>Status</label><input  placeholder="Status" ref={status=>this.status=status}/></div>              
            <div class="inline field"><label>Category</label><input  placeholder="Category" ref={category=>this.category=category}/></div>              
          

           
          
            
            <div class=" field"><button  class="ui primary button" onClick={this.onSubmit}>Add Request </button></div>
           {/* </div> */}
            </form>
</div>

          
            
                 
                       
            </div>
        
    }
}