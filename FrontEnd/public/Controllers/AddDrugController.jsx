'use strict';

import React, {Component} from 'react';
import axios from 'axios';
var url = require('../../Statics.Common');

export default class AddDrugController extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            users: []
        }
            this.getAllUsers();
        
    }

   onSubmit(event){
        event.preventDefault();
        this.props.addDrug(
            this.drugid.value,
            this.drugname.value,
            this.drugqty.value,
            this.remark.value,
            this.drugtype.value,
            this.drugcreatedate.value,
            this.drugdosage.value,
            this.drugcreatedby.value,
            this.drugexpiration.value,
            this.drugprice.value,
            
        );
        this.drugid.value='';
        this.drugname.value='';
        this.drugqty.value='';
        this.remark.value='';
        this.drugtype.value='';
        this.drugcreatedate.value='';
        this.drugdosage.value='';
        this.drugcreatedby.value='';
        this.drugexpiration.value='';
        this.drugprice.value='';

        alert('Drug Added Successfully');
        
   }

    getAllUsers() {
        axios.get(url.node + `drug`).then(res => {
            this.setState({
                users: res.data.data || res.data
            });
        })
    }

    render() {
        return <div>
            <br/><br/><div class="ui grid" align="left">
            <div class="ui blue icon message">
  <i class="heartbeat icon"></i>
  <div class="content">
    <div class="header">
    Add New Drugs
    </div>
    <p>Health is not valued till sickness comes...</p>
  </div>
</div>



            <form class="ui  form">
            {/* <div class="inline fields"> */}
            
            <div class="inline field"><label>Drug ID</label><input  placeholder="Drug ID" ref={drugid=>this.drugid=drugid}/></div>
            <div class="inline field"><label>Drug Name</label><input  placeholder="Drug Name" required ref={drugname=>this.drugname=drugname}/></div>              
            <div class="inline field"><label>Quantity</label><input  type="number" placeholder="Quantity" ref={drugqty=>this.drugqty=drugqty}/></div>              
            <div class="inline field"><label>Remark</label><textarea rows="2" placeholder="Remark" ref={remark=>this.remark=remark}/></div>              
            <div class="inline field"><label>Drug Type</label><input  placeholder="Drug Type" ref={drugtype=>this.drugtype=drugtype}/></div>              
          

           
            <div class="inline field"><label>Created Date</label><input  placeholder="Created Date" ref={drugcreatedate=>this.drugcreatedate=drugcreatedate}/></div>              
            <div class="inline field"><label>Drug Dosage</label><input type="number" placeholder="Drug Dosage" ref={drugdosage=>this.drugdosage=drugdosage}/></div>
            <div class="inline field"><label>Drug Created</label><input  placeholder="Drug Created" ref={drugcreatedby=>this.drugcreatedby=drugcreatedby}/></div>
            <div class="inline field"><label>Drug Expiration</label><input  placeholder="Drug Expiration" ref={drugexpiration=>this.drugexpiration=drugexpiration}/></div>
            <div class="inline field"><label>Drug Price</label><input  placeholder="Drug Price" ref={drugprice=>this.drugprice=drugprice}/></div>
            
           
            
            <div class=" field"><button align="center" class="ui primary button" onClick={this.onSubmit}>Add Drug </button></div>
           {/* </div> */}
            </form>
</div>

          
            
                 
                       
            </div>
        
    }
}