'use strict';

import React, {Component} from 'react';
import axios from 'axios';

export default class BatchController extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            users: []
        }
        
    }

   onSubmit(event){
        event.preventDefault();
        this.props.addBatch(
            this.batchNumber.value,
            this.category.value,
            this.content.value,
            this.contentType.value,
            this.numberofcartoons.value,
            this.numberofcardspercartoon.value,
            this.numberoftabletspercard.value,
            this.manufactureddate.value,
            this.expiredate.value


            
        );
        this.batchNumber.value='';
        this.category.value='';
        this.content.value='';
        this.contentType.value='';
        this.numberofcartoons.value='';
        this.numberofcardspercartoon.value='';
        this.numberoftabletspercard.value='';
        this.manufactureddate.value='';
        this.expiredate.value='';
        alert('Batch Added Successfully');
        
   }

    

    render() {
        return <div>
            <br/><br/><div class="ui grid" align="left">
            <div class="ui blue icon message">
  <i class="medkit icon"></i>
  <div class="content">
    <div class="header">
    Add New Batch
    </div>
    <p>Health is not valued till sickness comes...</p>
  </div>
</div>



            <form class="ui  form">
            {/* <div class="inline fields"> */}
            <table class="ui  large teal celled table">

            <tr>
            <div class="inline field"><td><label>Batch Number</label>
            <input placeholder="Batch Number" ref={batchNumber=>this.batchNumber=batchNumber}/></td></div>
            </tr>

            <tr>
            <div class="inline field"><td><label>Category</label>
            <input  placeholder="Category" ref={category=>this.category=category}/></td></div>    </tr>      
            <tr>
            <div class="inline field"><td><label>Content</label>
            <input  placeholder="Content" ref={content=>this.content=content}/></td></div>   </tr>
            <tr>
            <div class="inline field"><td><label>Content Type</label>
             <input placeholder="Content Type" ref={contentType=>this.contentType=contentType}/></td></div>  </tr>    
            <tr>
            <div class="inline field"><td><label>Number Of Cartoons</label>
            <input type="number" placeholder="Number Of Cartoons" ref={numberofcartoons=>this.numberofcartoons=numberofcartoons}/></td></div>    </tr>
            <tr>
            <div class="inline field"><td><label>Number Of Cards Per Cartoon</label>
             <input type="number" placeholder="Number Of Cards Per Cartoon" ref={numberofcardspercartoon=>this.numberofcardspercartoon=numberofcardspercartoon}/></td></div>  </tr>
            <tr>
            <div class="inline field"><td><label>Number Of Tablets Per Card</label>
            <input type="number" placeholder="Number Of Tablets Per Card" ref={numberoftabletspercard=>this.numberoftabletspercard=numberoftabletspercard}/></td></div> </tr>
            <tr>
            <div class="inline field"><td><label>Manufactured Date</label>
            <input  placeholder="Manufactured Date" ref={manufactureddate=>this.manufactureddate=manufactureddate}/></td></div> </tr>
            <tr>
            <div class="inline field"><td><label>Expire Date</label>
            <input  placeholder="Expire Date" ref={expiredate=>this.expiredate=expiredate}/></td></div> </tr>
            
           
            <tr>
            <td><div class=" field"><button class="ui primary button" onClick={this.onSubmit}>Add Batch</button></div> </td></tr>
           {/* </div> */}</table>
            </form>
</div>

          
            
                 
                       
            </div>
        
    }
}