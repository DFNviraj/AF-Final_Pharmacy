// import React from "react";

// export default () => {
//   return (
//     <footer className="bg-dark text-white mt-111 p-4 text-center">
//       Copyright &copy; {new Date().getFullYear()} Team Dual Boot
//     </footer>
//   );
// };
import React from 'react';

class FooterPage extends React.Component {
    render(){
        return(
          <div class="ui inverted vertical footer segment form-page">
          <div class="ui container">
           <div align="center">Team Dual Boot 2018 .&copy; All Rights Reserved</div>
          </div>
        </div>
        );
    }
}

export default FooterPage;