'use strict';

import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

import DrugHandler from './Views/DrugHandler';
import UpdateDrugHandler from './Views/UpdateDrugHandler';
import DrugInformation from './Views/DrugInformation';
import RequestInformation from './Views/RequestInformation';
import FooterPage from './Layout/Footer';
import BatchInformation from './Views/BatchInformation';

//UpdateSupplierHandler
//SupplierInformation
import SupplierInformation from './Views/SupplierInformation';
import SupplierHandler from './Views/SupplierHandler';

import Home from './Views/Home';
import Footer from './Layout/Footer';

import "./App.css";
import UpdateSupplierHandler from "./Views/UpdateSupplierHandler";

export default class AppContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div>
            <br/>
                    <div className="pusher" align="right">
                    <div className="ui grid container">
                    <div className="computer tablet only row" stylename="margin-top: 100px">
                            <div className="ui fixed inverted menu navbar" stylename="height: 50px">
                                <a href="" className="active right item">Logged in as Saman</a><i class="first aid icon"></i>
                                <a href="" className="item">Logout</a>
                            </div>
                        </div>

                        <div className="computer tablet only row" stylename="margin-top: 500px">
                            <div className="ui fixed inverted menu navbar" stylename="height: 50px">
                                <a href="" className="active right item"><i class="first aid icon"></i>Logged in as Saman</a>
                                <a href="" className="item">Logout</a>
                            </div>
                        </div>

        <div className="ui left fixed vertical inverted menu">
        <div className="ui inverted message" stylename="border-radius:0px">
        <div align="right"><i align="center" class="large ambulance icon"></i><i class="large heartbeat icon"></i><i class="large first aid icon"></i>
        <i class="large stethoscope icon"></i></div><br/><br/>
        <div className="header">
        <h2 align="center"><b stylename="color:#f74e4e">Lanka Suwaya Pharmacy</b></h2></div>
        </div>
            <br/>
           
            <br/><br/>

        <a className="active gray item" align="left">
            Home For Pharmacy
        </a>

        

      </div>




        </div>

</div>

{/* <br/>
<ol class="breadcrumb">
  <h3>Pharmacy</h3>
</ol> */}




<br/><br/>

            <Router>
                <div align="center">
				<ul className="nav nav-tabs justify-content-center">
                    <div className="row">
						<li className="nav-item">
                    <div className="nav-link active" data-toggle="tab">
                     <Link to="/">Home</Link></div>
                    </li>
                    <li className="nav-item">
                    <div className="nav-link" data-toggle="tab">
                    <Link to="/adddrugs">Add Drugs</Link></div>
                    </li>


                    <li className="nav-item">
                    <div className="nav-link" data-toggle="tab">
                    <Link to="/updatedrugs">Update Drugs</Link></div>
                    </li>

                   

                    <li className="nav-item">
                    <div className="nav-link" data-toggle="tab">
                    <Link to="/request">Requests</Link></div>
                    </li>

                   

                    <li className="nav-item">
                    <div className="nav-link" data-toggle="tab">
                    <Link to="/batches">Add Batches</Link></div>
                    </li>


                     <li className="nav-item">
                    <div className="nav-link" data-toggle="tab">
                    <Link to="/viewdrugs">Drug Infor</Link></div>
                    </li>
                      <li className="nav-item">
                        <div className="nav-link" data-toggle="tab">
                          <Link to="/supplier">Suppliers</Link></div>
                      </li>
                      <li className="nav-item">
                        <div className="nav-link" data-toggle="tab">
                          <Link to="/viewsupplier">View Suppliers</Link></div>
                      </li>
                      <li className="nav-item">
                        <div className="nav-link" data-toggle="tab">
                          <Link to="/updatesupplier">Update Suppliers</Link></div>
                      </li>

                  


                    </div>
                    <Route exact path="/" render={props => {
                        return <Home/>
                    }}/>
                    <Route path="/adddrugs" render={props => {
                        return <DrugHandler/>
                    }}/>
                      <Route path="/viewdrugs" render={props => {
                        return <DrugInformation/>
                    }}/>

                     <Route path="/request" render={props => {
                        return <RequestInformation/>
                    }}/>

                      <Route path="/batches" render={props => {
                        return <BatchInformation/>
                    }}/>


                    <Route path="/updatedrugs" render={props => {
                        return <UpdateDrugHandler/>
                    }}/>

          <Route path="/supplier" render={props => {
            return <SupplierHandler/>
          }}/>
          <Route path="/viewsupplier" render={props => {
            return <SupplierInformation/>
          }}/>

          <Route path="/updatesupplier" render={props => {
            return <UpdateSupplierHandler/>
          }}/>

					</ul>
                </div>
            </Router>

            <br/><br/><br/>
            <FooterPage/>
        </div>;

    }
}
