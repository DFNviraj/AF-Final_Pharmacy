'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import AddDrugController from '../Controllers/AddDrugController';
var url = require('../../Statics.Common');

export default class DrugHandler extends Component {
   constructor(props) {
      super(props);
      this.state = {
         drugs: []
      };
      this.addDrug = this.addDrug.bind(this);
   }

   addDrug(drugid,drugname,drugqty,remark,drugtype,drugcreatedate,drugdosage,drugcreatedby,drugexpiration,drugprice){
    axios.post(url.node + `drug`,{
        drugid:drugid,
        drugname:drugname,
        drugqty:drugqty,
        remark:remark,
        drugtype:drugtype,
        drugcreatedate:drugcreatedate,
        drugdosage:drugdosage,
        drugcreatedby:drugcreatedby,
        drugexpiration:drugexpiration,
        drugprice:drugprice
      

    }).then(res=>{
        console.log(res);
    })
}



   render() {
      return <div>
           <AddDrugController
                addDrug={this.addDrug}
            />

       
      </div>
   }
}