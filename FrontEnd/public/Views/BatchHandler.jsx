'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import BatchController from '../Controllers/BatchController';
var url = require('../../Statics.Common');


export default class BatchHandler extends Component {
   constructor(props) {
      super(props);
      this.state = {
         batch: []
      };
      this.addBatch = this.addBatch.bind(this);
   }

   addBatch(batchNumber,
    category,
    content,
    contentType,
    numberofcartoons,
    numberofcardspercartoon,
    numberoftabletspercard,
    manufactureddate,
    expiredate){
    axios.post(url.node + `batches`,{
        batchNumber:batchNumber,
        category:category,
        content:content,
        contentType:contentType,
        numberofcartoons:numberofcartoons,
        numberofcardspercartoon:numberofcardspercartoon,
        numberoftabletspercard:numberoftabletspercard,
        manufactureddate:manufactureddate,
        expiredate:expiredate
      

    }).then(res=>{
        console.log(res);
    })
}



   render() {
      return <div>
           <BatchController
                addBatch={this.addBatch}
            />

       
      </div>
   }
}