'use strict';

import React, {Component} from 'react';

export default class Dashboard extends Component {

    render() {
        return <div>
            
<div align="right">
<div class="ui six column row" >


<div class="dashboard-stat column">
  <div class="ui segments">
    <div class="ui secondary black inverted dashboard center aligned segment">
      <div class="ui dashboard statistic">
        <div class="value">
          80
        </div>
        <div class="label">
          No of Requests
        </div>

      </div>
    </div>
  </div>
</div>

<div class="dashboard-stat column">
    <div class="ui segments">
      <div class="ui secondary black inverted dashboard center aligned segment">
        <div class="ui dashboard statistic">
          <div class="value">
            110
          </div>
          <div class="label">
            Stock Remains
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="dashboard-stat column">
      <div class="ui segments">
        <div class="ui secondary black inverted dashboard center aligned segment">
          <div class="ui dashboard statistic">
            <div class="value">
              1
            </div>
            <div class="label">
              No of Batches
            </div>
  
          </div>
        </div>
      </div>
    </div>

    <div class="dashboard-stat column">
        <div class="ui segments">
          <div class="ui secondary black inverted dashboard center aligned segment">
            <div class="ui dashboard statistic">
              <div class="value">
                10
              </div>
              <div class="label">
                Inward Patients
              </div>
    
            </div>
          </div>
        </div>
      </div>

      <div class="dashboard-stat column">
          <div class="ui segments">
            <div class="ui secondary black inverted dashboard center aligned segment">
              <div class="ui dashboard statistic">
                <div class="value">
                  2
                </div>
                <div class="label">
                  Expired Stock
                </div>
      
              </div>
            </div>
          </div>
        </div>

        <div class="dashboard-stat column">
            <div class="ui segments">
              <div class="ui secondary black inverted dashboard center aligned segment">
                <div class="ui dashboard statistic">
                  <div class="value">
                    5
                  </div>
                  <div class="label">
                    Connected Patients
                  </div>
        
                </div>
              </div>
            </div>
          </div>


    

  
   
 
</div>  </div>


            </div>
    }
}
