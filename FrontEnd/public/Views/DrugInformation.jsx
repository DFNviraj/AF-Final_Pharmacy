'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
var url = require('../../Statics.Common');

export default class DrugInformation extends Component {
  
   constructor(props) {
      super(props);
   

      this.state = {
         drugs: [],
         users:[],
      }; 
      
   }

  
   delete(id) {
    axios.delete(url.node + `drug/` + id).then(results => {
        if(results.status == 200) {
            // this.getAllUsers();
        }
    })
}




   componentWillMount() {
      axios.get(url.node + `drug`).then(res => {
         const drugs = res.data;
         this.setState({ drugs:drugs.data });
         console.log(drugs);

      })
   }

   render() {
    return <div>
          <br/>
          <div class="ui blue icon message">
  <i class="hospital outline icon"></i>
  <div class="content">
    <div class="header">
     Drug Information
    </div>
    <p>Health is the thing that makes you feel that now is the best time of the year...</p>
  </div>
</div>

         <table  class="ui celled table"  width="200">
         <thead>
    <tr>
        <th width="20">DrugID</th>
    <th width="20">Drug Name</th>
    <th width="20">Quantity</th>
    <th width="80">Remark</th>
    <th width="20">Drug Type</th>
    <th width="20">Created Date</th>
    <th width="20">Drug Created By</th>
    <th width="20">Drug Expiration</th>
    <th width="20">Drug Price</th>
  </tr></thead> </table>
         <div>
            {
               this.state.drugs.map(drug =>
                  <div>
                      <table  key={drug._id} class="ui  large  celled table"> 
                      
                        <tr>
                           
                           <td>{drug.drugid}</td>
                           <td > {drug.drugname}</td>
                           <td >{drug.drugqty}</td>
                           <td >{drug.remark}</td>
                           <td >{drug.drugtype}</td>
                           <td >{drug.drugcreatedate}</td>
                           <td >{drug.drugdosage}</td>
                           <td >{drug.drugcreatedby}</td>
                           <td >{drug.drugexpiration}</td>
                           <td >{drug.drugprice}</td>
                           <td ><button class= "negative ui button" onClick={(e) => this.delete(drug._id)}>Delete</button></td>
                         
                        </tr>  
                        </table>
                  </div>
               )
            } 
         </div> 

         <br/>   <br/>   <br/>   <br/>   <br/>   <br/><br/>   <br/>   <br/>   <br/>   <br/>   <br/>
      </div>
   }
}