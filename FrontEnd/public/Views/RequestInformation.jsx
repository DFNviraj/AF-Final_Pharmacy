'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import RequestHandler from './RequestHandler';
var url = require('../../Statics.Common');

export default class RequestInformation extends Component {
   
   constructor(props) {
      super(props);

      this.state = {
         drugReq: [],
         users:[],
      }; 
      
   }

  
   delete(id) {
    axios.delete(url.spring + `drugrequest/` + id).then(results => {
        if(results.status == 200) {
            // this.getAllUsers();
        }
    })
}



   componentWillMount() {
      axios.get(url.spring + `drugrequest`).then(res => {
         const drugReq = res.data;
         this.setState({ drugReq });
         console.log(drugReq);

      })
   }

   render() {
    return <div>
        <RequestHandler/>
          <br/>
          <div class="ui blue icon message">
  <i class="calendar alternate icon"></i>
  <div class="content">
    <div class="header">
    Request Information
    </div>
    <p>Good health and good sense are two of life's greatest blessings...</p>
  </div>
</div>

         <table  class="ui celled table"  width="200">
         <thead>
    <tr>
        <th width="20">DrugID</th>
    <th width="20">Drug Name</th>
    <th width="20">Requested Quantity</th>
    <th width="20">Department</th>
    <th width="20">Status</th>
    <th width="20">Category</th>
 
  </tr></thead> </table>
         <div>
            {
               this.state.drugReq.map(drugRequest =>
                  <div>
                      <table  key={drugRequest.id} class="ui  large  celled table">
                      
                        <tr>
                           
                           <td >{drugRequest.drugid}</td>
                           <td > {drugRequest.drugName}</td>
                           <td >{drugRequest.requestedQty}</td>
                           <td >{drugRequest.department}</td>
                           <td >{drugRequest.status}</td>
                           <td >{drugRequest.category}</td>
           
                           <td ><button class= "negative ui button" onClick={(e) => this.delete(drugRequest.id)}>Delete</button></td>
                         
                        </tr>  
                        </table>
                  </div>
               )
            } 
         </div> 
      </div>
   }
}