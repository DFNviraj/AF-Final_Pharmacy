'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import AddSupplierController from '../Controllers/AddSupplierController';
var url = require('../../Statics.Common');

export default class SupplierHandler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      suppliers: []
    };
    this.addSupplier = this.addSupplier.bind(this);
  }

  addSupplier(supplierid,suppliername,suppliercompany,supplieremail,suppliercontactno,supplieraddress){
    axios.post(url.node + `supplier`,{
      supplierid:supplierid,
      suppliername:suppliername,
      suppliercompany:suppliercompany,
      supplieremail:supplieremail,
      suppliercontactno:suppliercontactno,
      supplieraddress:supplieraddress

    }).then(res=>{
      console.log(res);
    })
  }





  render() {
    return <div>
      <AddSupplierController
        addSupplier={this.addSupplier}
      />


    </div>
  }
}
