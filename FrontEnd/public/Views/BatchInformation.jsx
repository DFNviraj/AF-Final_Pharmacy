'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
var url = require('../../Statics.Common');
import BatchHandler from './BatchHandler';

export default class BatchInformation extends Component {
   
   constructor(props) {
      super(props);

      this.state = {
         batch: [],
         users:[],
      }; 
      
   }

  
   delete(id) {
    axios.delete(url.node + `batches/` + id).then(results => {
        if(results.status == 200) {
            // this.getAllUsers();
        }
    })
}



   componentWillMount() {
      axios.get(url.node + `batches`).then(res => {
         const batch = res.data;
         this.setState({ batch:batch.data });
         console.log(batch);

      })
   }

   render() {
    return <div>
        <BatchHandler/>
          <br/>
          <div class="ui blue icon message">
  <i class="user md icon"></i>
  <div class="content">
    <div class="header">
    Batch Information
    </div>
    <p>Maintaining good health should be the primary focus of everyone...</p>
  </div>
</div>

         <table  class="ui  large teal celled table">
         <thead>
    <tr>
        <th width="20">Batch Number</th>
    <th width="20">Category</th>
    <th width="20">Content</th>
    <th width="20">Content Type</th>
    <th width="20">Number of cartoons</th>
    <th width="20">Number of cards per cartoon</th>
    <th width="20">Number of tablets per card</th>
    <th width="20">ManufacturedDdate</th>
    <th width="20">Expire Date</th>
  </tr></thead> </table>
         <div>
            {
               this.state.batch.map(batchs =>
                  <div key={batchs._id}>
                      <table   class="ui  large  celled table">
                      
                        <tr>
                           
                           <td >{batchs.batchNumber}</td>
                           <td >{batchs.category}</td>
                           <td >{batchs.content}</td>
                           <td>{batchs.contentType}</td>
                           <td >{batchs.numberofcartoons}</td>
                           <td >{batchs.numberofcardspercartoon}</td>
                           <td >{batchs.numberoftabletspercard}</td>
                           <td >{batchs.manufactureddate}</td>
                           <td >{batchs.expiredate}</td>
                           
                           <td><button class= "negative ui button" onClick={(e) => this.delete(batchs._id)}>Delete</button></td>
                         
                        </tr>  
                        </table>
                  </div>
               )
            } 
         </div> 
      </div>
   }
}