'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import UpdateSupplierController from '../Controllers/UpdateSupplierController';
import PropTypes            from 'prop-types';
var url = require('../../Statics.Common');

export default class UpdateSupplierHandler extends Component {
 
  constructor(props) {
    super(props);

    this.state = {
      suppliers: []
    };
    this.updateSupplier = this.updateSupplier.bind(this);
  }

  updateSupplier(supplierid,suppliername,suppliercompany,supplieremail,suppliercontactno,supplieraddress){
    axios.put(url.node + `supplier/` + suppliername ,{
      supplierid:supplierid,
      suppliername:suppliername,
      suppliercompany:suppliercompany,
      supplieremail:supplieremail,
      suppliercontactno:suppliercontactno,
      supplieraddress:supplieraddress


   
    }).then(res=>{
      console.log(res);
    })
  }



  render() {
    return <div>
      <UpdateSupplierController
        updateSupplier={this.updateSupplier}
      />


    </div>
  }
}
