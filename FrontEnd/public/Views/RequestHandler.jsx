'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import AddRequestController from '../Controllers/AddRequestController';
var url = require('../../Statics.Common');
export default class RequestHandler extends Component {
   constructor(props) {
      super(props);
      this.state = {
         drugReq: []
      };
      this.addRequest = this.addRequest.bind(this);
   }

   addRequest(drugid,drugName,requestedQty,department,status,category){
    axios.post(url.spring + `drugrequest`,{
        drugid:drugid,
        drugName:drugName,
        requestedQty:requestedQty,
        department:department,
        status:status,
        category:category
      

    }).then(res=>{
        console.log(res);
    })
}



   render() {
      return <div>
           <AddRequestController
                addRequest={this.addRequest}
            />

       
      </div>
   }
}