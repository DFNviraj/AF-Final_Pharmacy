'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
var url = require('../../Statics.Common');

export default class SupplierInformation extends Component {
  // static get propTypes() {
  //     return {
  //         getAllUsers: PropTypes.func
  //     }
  // }
  constructor(props) {
    super(props);
    //   this.getAllUsers = this.props.getAllUsers;

    this.state = {
      suppliers: [],
      users:[],
    };

  }


  delete(id) {
    axios.delete(url.node + `supplier/` + id).then(results => {
      if(results.status == 200) {
        // this.getAllUsers();
      }
    })
  }


  componentWillMount() {
    axios.get(url.node + `supplier`).then(res => {
      const suppliers = res.data;
      this.setState({ suppliers:suppliers.data });
      console.log(suppliers);

    })
  }

  render() {
    return <div>
      <br/>
      <div class="ui blue icon message">
        <i class="handshake icon"></i>
        <div class="content">
          <div class="header">
           Supplier Information
          </div>
          <p>Maintaining good health should be the primary focus of everyone...</p>
        </div>
      </div>

      <table  class="ui celled table"  width="200">
        <thead>
        <tr>
          <th width="20">Supplier ID</th>
          <th width="20">Supplier Name</th>
          <th width="20">Supplier Company</th>
          <th width="20">Supplier Email</th>
          <th width="20">Supplier Contact No</th>
          <th width="20">Supplier Address</th>
        </tr></thead> </table>
      <div>
        {
          this.state.suppliers.map(supplier =>
            <div>
              <table  key={supplier._id} class="ui  large  celled table">

                <tr>

                  <td >{supplier.supplierid}</td>
                  <td > {supplier.suppliername}</td>
                  <td >{supplier.suppliercompany}</td>
                  <td >{supplier.supplieremail}</td>
                  <td >{supplier.suppliercontactno}</td>
                  <td >{supplier.supplieraddress}</td>


                  <td width="20"><button class= "negative ui button" onClick={(e) => this.delete(supplier._id)}>Delete</button></td>

                </tr>
              </table>
            </div>
          )
        }
      </div><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    </div>
  }
}
