'use strict';

import React, { Component } from 'react';
import axios from 'axios';
import UpdateDrugController from '../Controllers/UpdateDrugController';
import PropTypes            from 'prop-types';
var url = require('../../Statics.Common');

export default class UpdateDrugHandler extends Component {
   
   constructor(props) {
      super(props);

      this.state = {
         drugs: []
      };
      this.updateDrug = this.updateDrug.bind(this);
   }

   updateDrug(drugid,drugname,drugqty,remark,drugtype,drugcreatedate,drugdosage,drugcreatedby,drugexpiration,drugprice){
    axios.put(url.node + `drug/` + drugname ,{
        drugid:drugid,
        drugname:drugname,
        drugqty:drugqty,
        remark:remark,
        drugtype:drugtype,
        drugcreatedate:drugcreatedate,
        drugdosage:drugdosage,
        drugcreatedby:drugcreatedby,
        drugexpiration:drugexpiration,
        drugprice:drugprice
      

    }).then(res=>{
        console.log(res);
    })
}



   render() {
      return <div>
           <UpdateDrugController
                updateDrug={this.updateDrug}
            />

       
      </div>
   }
}