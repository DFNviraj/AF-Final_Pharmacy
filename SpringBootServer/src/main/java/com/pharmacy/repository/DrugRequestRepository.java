package com.pharmacy.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.pharmacy.model.DrugRequest;

@Repository
public interface DrugRequestRepository extends MongoRepository<DrugRequest, String> {

    List<DrugRequest> findByDeleted(boolean deleted);
//    
    List<DrugRequest> findByCategoryAndDeleted(String status, boolean deleted);
}
