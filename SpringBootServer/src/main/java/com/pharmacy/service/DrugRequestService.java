package com.pharmacy.service;

import java.util.List;

import com.pharmacy.model.DrugRequest;

public interface DrugRequestService {

    DrugRequest createDrugRequest(DrugRequest drugRequest);

    List<DrugRequest> listDrugRequests(String status);

    DrugRequest updateDrugRequest(String id, DrugRequest drugRequest);

    DrugRequest retrieveDrugRequest(String id);
    
    List<DrugRequest> retrieveAllDrugRequest();

    void deleteDrugRequest(String id);
    
}
