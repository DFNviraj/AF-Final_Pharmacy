package com.pharmacy.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pharmacy.model.DrugRequest;
import com.pharmacy.repository.DrugRequestRepository;
import com.pharmacy.service.DrugRequestService;

@Service
public class DrugRequestServiceImpl implements DrugRequestService {

    @Autowired
    private DrugRequestRepository drugRequestRepository;

	@Override
	public DrugRequest createDrugRequest(DrugRequest drugRequest) {
		drugRequest.setId(UUID.randomUUID().toString());
		return drugRequestRepository.save(drugRequest);
	}

	@Override
	public List<DrugRequest> listDrugRequests(String status) {
		return drugRequestRepository.findByCategoryAndDeleted(status, false);
	}

	@Override
	public DrugRequest updateDrugRequest(String id, DrugRequest drugRequest) {
		return drugRequestRepository.save(drugRequest);
	}

	@Override
	public DrugRequest retrieveDrugRequest(String id) {
		 return drugRequestRepository.findOne(id);
	}

	@Override
	public void deleteDrugRequest(String id) {
        final DrugRequest drugReq = drugRequestRepository.findOne(id);
        drugReq.setDeleted(true);
        drugRequestRepository.delete(drugReq);
		
	}

	@Override
	public List<DrugRequest> retrieveAllDrugRequest() {
		return drugRequestRepository.findAll();
}
}
