package com.pharmacy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pharmacy.model.DrugRequest;
import com.pharmacy.service.DrugRequestService;

@RestController
@RequestMapping("/drugrequest")
public class DrugRequestController {

    
    @Autowired
    private DrugRequestService drugRequestService;

    @RequestMapping(method = RequestMethod.POST)
    public DrugRequest createDrugRequest(@Validated @RequestBody final DrugRequest drugRequest) {
        return drugRequestService.createDrugRequest(drugRequest);
    }

    
    @RequestMapping(value = "/{status}",method = RequestMethod.GET)
    public List<DrugRequest> listDrugRequest(@PathVariable("status") final String status) {
        return drugRequestService.listDrugRequests(status);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<DrugRequest> retrieveAllDrugRequest() {
        return drugRequestService.retrieveAllDrugRequest();
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public DrugRequest updateDrugRequest(@PathVariable("id") final String id, @Validated @RequestBody final DrugRequest drugRequest) {
        return drugRequestService.updateDrugRequest(id, drugRequest);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DrugRequest getDrugRequest(@PathVariable("id") final String id) {
        return drugRequestService.retrieveDrugRequest(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteDrug(@PathVariable("id") final String id) {
    	drugRequestService.deleteDrugRequest(id);
    }
}
