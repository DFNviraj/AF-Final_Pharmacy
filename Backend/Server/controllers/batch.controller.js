var mongoose    = require('../mongoose.config');
var BatchSchema 	= mongoose.model('Batch');
var BatchController = function() {

	this.add = function(batchInstance) {
		return new Promise((resolve, reject) => {
			var batch = new BatchSchema({
				batchNumber: batchInstance.batchNumber,
				category: batchInstance.category,
				content: batchInstance.content,
				contentType: batchInstance.contentType,
				numberofcartoons: batchInstance.numberofcartoons,
				numberofcardspercartoon: batchInstance.numberofcardspercartoon,
				numberoftabletspercard: batchInstance.numberoftabletspercard,
				manufactureddate: batchInstance.manufactureddate,
				expiredate:  batchInstance.expiredate


            })
			batch.save().then(() => {
				resolve({'status': 200, 'message':'Batch Added'});
			}).catch(err => {
				reject({'status': 404, 'message':'err:-'+err});
			})
		})
	}

	this.getAll = function() {
		return new Promise((resolve, reject) => {
			BatchSchema.find().exec().then(data => {
                resolve({'status': 200, 'message':'get all data', 'data': data});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
			})
		})
	}

	this.getSingle = function(id) {
		return new Promise((resolve, reject) => {
			BatchSchema.find({_id: id}).exec().then(data => {
                resolve({'status': 200, 'message':'get single data', 'data': data});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

	this.update = function(id, updateData) {
		return new Promise((resolve, reject) => {
			BatchSchema.update({_id: id}, updateData).then(() => {
                resolve({'status': 200, 'message':'Batch Updated'});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

	this.delete = function(id) {
		return new Promise((resolve, reject) => {
			BatchSchema.remove({_id: id}).then(() => {
                resolve({'status': 200, 'message':'Drug Deleted'});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

}

module.exports = new BatchController();