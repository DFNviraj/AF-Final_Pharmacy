var mongoose    = require('../mongoose.config');
var SupplierSchema 	= mongoose.model('Supplier');
var SupplierController = function() {

  this.add = function(supplierInstance) {
    return new Promise((resolve, reject) => {
      var supplier = new SupplierSchema({
        supplierid: supplierInstance.supplierid,
        suppliername: supplierInstance.suppliername,
        suppliercompany: supplierInstance.suppliercompany,
        supplieremail: supplierInstance.supplieremail,
        suppliercontactno: supplierInstance.suppliercontactno,
        supplieraddress: supplierInstance.supplieraddress,


      })
      supplier.save().then(() => {
      resolve({'status': 200, 'message':'Supplier Added'});
  }).catch(err => {
      reject({'status': 404, 'message':'err:-'+err});
  })
  })
  }

  this.getAll = function() {
    return new Promise((resolve, reject) => {
      SupplierSchema.find().exec().then(data => {
      resolve({'status': 200, 'message':'get all data', 'data': data});
  }).catch(err => {
      reject({'status': 404, 'message':'err:-'+err});
  })
  })
  }

  this.getSingle = function(id) {
    return new Promise((resolve, reject) => {
      SupplierSchema.find({_id: id}).exec().then(data => {
      resolve({'status': 200, 'message':'get single data', 'data': data});
  }).catch(err => {
      reject({'status': 404, 'message':'err:-'+err});
  })
  })
  }

  this.update = function(id, updateData) {
    return new Promise((resolve, reject) => {
      SupplierSchema.update({_id: id}, updateData).then(() => {
      resolve({'status': 200, 'message':'Supplier Updated'});
  }).catch(err => {
      reject({'status': 404, 'message':'err:-'+err});
  })
  })
  }

  this.delete = function(id) {
    return new Promise((resolve, reject) => {
      SupplierSchema.remove({_id: id}).then(() => {
      resolve({'status': 200, 'message':'Supplier Deleted'});
  }).catch(err => {
      reject({'status': 404, 'message':'err:-'+err});
  })
  })
  }

}

module.exports = new SupplierController();
