var mongoose    = require('../mongoose.config');
var DrugRequestSchema 	= mongoose.model('DrugRequest');
var DrugRequestController = function() {

	this.addRequest = function(drugReqInstance) {
		return new Promise((resolve, reject) => {
			var drugRequest = new DrugRequestSchema({
				drugid: drugReqInstance.drugid,
				drugName: drugReqInstance.drugName,
				requestedQty: drugReqInstance.requestedQty,
				department: drugReqInstance.department,
				status: drugReqInstance.status


            })
			drugRequest.save().then(() => {
				resolve({'status': 200, 'message':'Drug Request Added'});
			}).catch(err => {
				reject({'status': 404, 'message':'err:-'+err});
			})
		})
	}

	this.getAll = function() {
		return new Promise((resolve, reject) => {
			DrugRequestSchema.find().exec().then(data => {
                resolve({'status': 200, 'message':'get all data', 'data': data});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
			})
		})
	}

	this.getSingle = function(id) {
		return new Promise((resolve, reject) => {
			DrugRequestSchema.find({_id: id}).exec().then(data => {
                resolve({'status': 200, 'message':'get single data', 'data': data});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

	this.update = function(id, updateData) {
		return new Promise((resolve, reject) => {
			DrugRequestSchema.update({_id: id}, updateData).then(() => {
                resolve({'status': 200, 'message':'Drug Request Updated'});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

	this.delete = function(id) {
		return new Promise((resolve, reject) => {
			DrugSchema.remove({_id: id}).then(() => {
                resolve({'status': 200, 'message':'Drug Deleted'});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

}

module.exports = new DrugRequestController();