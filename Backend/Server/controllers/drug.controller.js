var mongoose    = require('../mongoose.config');
var DrugSchema 	= mongoose.model('Drug');
var DrugController = function() {

	this.add = function(drugInstance) {
		return new Promise((resolve, reject) => {
			var drug = new DrugSchema({
				drugid: drugInstance.drugid,
				drugname: drugInstance.drugname,
				drugqty: drugInstance.drugqty,
				remark: drugInstance.remark,
				drugtype: drugInstance.drugtype,
				drugcreatedate: drugInstance.drugcreatedate,
				drugdosage: drugInstance.drugdosage,
				drugcreatedby: drugInstance.drugcreatedby,
				drugexpiration:  drugInstance.drugexpiration,
				drugprice:  drugInstance.drugprice


            })
			drug.save().then(() => {
				resolve({'status': 200, 'message':'Drug Added'});
			}).catch(err => {
				reject({'status': 404, 'message':'err:-'+err});
			})
		})
	}

	this.getAll = function() {
		return new Promise((resolve, reject) => {
			DrugSchema.find().exec().then(data => {
                resolve({'status': 200, 'message':'get all data', 'data': data});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
			})
		})
	}

	this.getSingle = function(id) {
		return new Promise((resolve, reject) => {
			DrugSchema.find({_id: id}).exec().then(data => {
                resolve({'status': 200, 'message':'get single data', 'data': data});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

	this.update = function(drugname, updateData) {
		return new Promise((resolve, reject) => {
			DrugSchema.update({drugname: drugname}, updateData).then(() => {
                resolve({'status': 200, 'message':'Drug Updated'});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

	this.delete = function(id) {
		return new Promise((resolve, reject) => {
			DrugSchema.remove({_id: id}).then(() => {
                resolve({'status': 200, 'message':'Drug Deleted'});
			}).catch(err => {
                reject({'status': 404, 'message':'err:-'+err});
            })
		})
	}

}

module.exports = new DrugController();