const mongoose 	= require('mongoose');
const Schema 	= mongoose.Schema;

const DrugSchema = new Schema({
	drugid: {type: String,require: true},
	drugname: {type: String,require: true},
	drugqty: {type: String,require: true},
	remark: {type: String,require: true},
	drugtype: {type: String,require: true},
	drugcreatedate: {type: String,require: true},
	drugdosage: {type: String,require: true},
	drugcreatedby: {type: String,require: true},
	drugexpiration: {type: String,require: true},
	drugprice: {type: String,require: true}

});

const SupplierSchema = new Schema({
  supplierid: {type: String,require: true},
  suppliername: {type: String,require: true},
  suppliercompany: {type: String,require: true},
  supplieremail: {type: String,require: true},
  suppliercontactno: {type: String,require: true},
  supplieraddress: {type: String,require: true}


});

const BatchSchema = new Schema({
	batchNumber: {type: String,require: true},
	category: {type: String,require: true},
	content: {type: String,require: true},
	contentType: {type: String,require: true},
	numberofcartoons: {type: String,require: true},
	numberofcardspercartoon: {type: String,require: true},
	numberoftabletspercard: {type: String,require: true},
	manufactureddate: {type: String,require: true},
	expiredate: {type: String,require: true}
  
  
  });
  

// const DrugRequestSchema = new Schema({
// 	drugid: {type: String,require: true},
// 	drugName: {type: String,require: true},
// 	requestedQty: {type: String,require: true},
// 	department: {type: String,require: true},
// 	status: {type: String,require: true}
    
// });

mongoose.model('Drug', DrugSchema);

// mongoose.model('DrugRequest', DrugRquestSchema);

mongoose.model('Supplier', SupplierSchema);
mongoose.model('Batch', BatchSchema);


mongoose.connect('mongodb://localhost:27017/pharmacy-module', (err) => {
	if (err) {
		console.log(err);
		process.exit(-1);
	}
	console.log('Connected to the DB');
});

module.exports = mongoose;
