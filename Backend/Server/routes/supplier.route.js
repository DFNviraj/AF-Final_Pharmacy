var express     = require('express');
var supplierrouter  = express.Router();
var controller	= require('../controllers/supplier.controller');
//
supplierrouter.post('/supplier', (req, res) => {
  controller.add(req.body).then(response => {
  res.status(response.status).send(response.message);
}).catch(err => {
  res.status(err.status).send(err.message);
})
});

supplierrouter.get('/supplier', (req, res) => {
  controller.getAll().then(response => {
  res.status(response.status).send(response);
}).catch(err => {
  res.status(err.status).send(err.message);
})
});

supplierrouter.get('/supplier/:id', (req, res) => {
  controller.getSingle(req.params.id).then(response => {
  res.status(response.status).send(response);
}).catch(err => {
  res.status(err.status).send(err.message);
})
});

supplierrouter.put('/supplier/:suppliername', (req, res) => {
  controller.update(req.params.suppliername, req.body).then(response => {
  res.status(response.status).send(response);
}).catch(err => {
  res.status(err.status).send(err.message);
})
});

supplierrouter.delete('/supplier/:id', (req, res) => {
  controller.delete(req.params.id).then(response => {
  res.status(response.status).send(response);
}).catch(err => {
  res.status(err.status).send(err.message);
})
})

module.exports = supplierrouter;
